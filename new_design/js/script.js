/**
 * Created by Doncatas on 17/10/2016.
 */
$(document).ready(function() {
    /* modal window */
    $(document).on('click', '.modal-body', function() {
       $(this).closest('.modal-bg').fadeOut('slow');
    });

    $(document).on('click', '.ui-toggleMenu', function() {
       $('.mobile-menu').fadeIn('slow');
    });

    /* close mobile menu */
    $(document).on('click', '.mobile-menu', function (e) {
        if(e.target == this) {
            $(this).fadeOut('slow');
        }
    });

    ///* set active icon for cloud menu */
    //function setActiveIcon() {
    //    if($('.ui-menu-icons>li.active>a')) {
    //        var iconItem = $('.ui-menu-icons>li.active>a').find('.ui-icon');
    //        var itemClass = iconItem.attr('class').split(' ')[1];
    //        iconItem.removeClass(itemClass);
    //        itemClass += "-filled";
    //        iconItem.addClass(itemClass);
    //    }
    //}
    //if($('.ui-menu-icons')) {
    //    setActiveIcon();
    //}

    //$(document).on('click', '.files-list .row', function() {
    //    if(!$(this).hasClass('header')) {
    //        $(this).toggleClass('active');
    //        var totalFoldersSelected = $(this).parent().children('.folder.active').length;
    //        var totalFilesSelected = $(this).parent().children('.file.active').length;
    //        var arrayOptions = ['.ui-cloud-menu-noselected', '.ui-cloud-menu-selected-folder', '.ui-cloud-menu-selected-file', '.ui-cloud-menu-selected-folders', '.ui-cloud-menu-selected-files', '.ui-cloud-menu-selected-folders-files'];
    //        $.each(arrayOptions, function(id, val) {
    //            $(val).hide();
    //        });

    //        if(totalFoldersSelected == 0 && totalFilesSelected == 0)
    //            $('.ui-cloud-menu-noselected').show();
    //        if(totalFoldersSelected == 1 && totalFilesSelected == 0)
    //            $('.ui-cloud-menu-selected-folder').show();
    //        if(totalFoldersSelected == 0 && totalFilesSelected == 1)
    //            $('.ui-cloud-menu-selected-file').show();
    //        if(totalFoldersSelected > 1 && totalFilesSelected == 0)
    //            $('.ui-cloud-menu-selected-folders').show();
    //        if(totalFoldersSelected == 0 && totalFilesSelected > 1)
    //            $('.ui-cloud-menu-selected-files').show();
    //        if(totalFoldersSelected > 0 && totalFilesSelected > 0)
    //            $('.ui-cloud-menu-selected-folders-files').show();
    //    }
    //});

    //$('.ui-cloud-rename').on('click', function() {
    //    var selectedItem = $('.files-list>.row.active').children();
    //    var type = $($(selectedItem[0]).html()).attr('class');
    //    var name = $(selectedItem[1]).html();

    //    if(!$(name).length) {

    //        var placeholder;
    //        if (type == 'ic-folder') {
    //            placeholder = "Folder name";
    //        }
    //        if (type == 'ic-file') {
    //            placeholder = "File name";
    //        }

    //        $(selectedItem[1]).html('<input type="text" placeholder="' + placeholder + '" value="' + $.trim(name) + '"/>');
    //    }
    //});
    
    //$('.ui-cloud-create-folder').on('click', function() {
    //    console.log('test');
    //   $('<div class="row folder"><div class="col-xs-1"><div class="ic-folder"></div></div><div class="col-xs-8 name"><input type="text" placeholder="Folder name" /></div><div class="col-xs-2"></div><div class="col-xs-1"></div></div>').insertAfter('.files-list>.row.header');
    //});

    var folderTemplate = '<tr><td class="type"><div class="ic-folder"></div></td><td>File name</td><td>-</td><td>-</td></tr>';
    $(document).on('click', '.c-create-folder', function(){
        $('.ui-cloud-table tbody').append(folderTemplate);
    });

    $('.search>input').focus(function(){
        $(this).addClass('search-active');
        $(this).next().addClass('search-btn-active');
        $(this).next().children('.bt-search2').addClass('bt-search2-active');
    });

    $('.search>input').focusout(function(){
        $(this).removeClass('search-active');
        $(this).next().removeClass('search-btn-active');
        $(this).next().children('.bt-search2').removeClass('bt-search2-active');
    });

    //Search button hover
    $(document).on('mouseenter', '.search-btn', function(){
        $('.search>input').addClass('search-active');
        $(this).addClass('search-btn-active');
        $(this).children('.bt-search2').addClass('bt-search2-active');
    });

    $(document).on('mouseleave', '.search-btn', function(){
        $('.search>input').removeClass('search-active');
        $(this).removeClass('search-btn-active');
        $(this).children('.bt-search2').removeClass('bt-search2-active');
    });


    //$('.selectpicker').selectpicker({
    //    style: 'btn-select'
    //});

});
